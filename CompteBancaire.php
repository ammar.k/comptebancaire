

<?php
    class CompteBancaire {
        private string $_libelle;
        private int $_solde = 0;
        private string $_devise;
        private TitulaireCompte $_titulaire;

        public function __construct(string $libelle, int $solde, string $devise, TitulaireCompte $titulaire)
        {
                $this->_libelle = $libelle;
                $this->_solde = $solde;
                $this->_devise = $devise;
                $this->_titulaire = $titulaire;
                $this->_titulaire->addCompte($this);

        }
     
        public function getLIbelle(): string
        {
                return $this->_libelle;
        }

        public function setLIbelle(string $libelle): self
        {
                $this->_libelle = $libelle;

                return $this;
        }


        public function getSoldeInitial(): int
        {
                return $this->_solde;
        }

        public function setSoldeInitial(int $solde): self
        {
                $this->_solde = $solde;

                return $this;
        }

        public function getDevise(): string
        {
                return $this->_devise;
        }


        public function setDevise(string $devise): self
        {
                $this->_devise = $devise;

                return $this;
        }

 
        public function getTitulaire()
        {
                return $this->_titulaire;
        }

        public function setTitulaire(string $titulaire): self
        {
                $this->_titulaire = $titulaire;

                return $this;
        }

        public function crediter(int $montant)
        {
                $this->_solde += $montant;
                return $this;
        }

        public function debiter(int $montant)
        {
                $this->_solde -= $montant;
                return $this;
        }

        public function virement(CompteBancaire $compte, int $montant)
        {
                $this->debiter($montant);
                $compte->crediter($montant);
        }
    

        public function __toString()
        {
                return $this->getLIbelle() .
                " le solde est de " . $this->getSoldeInitial() . " " . $this->getDevise() . "<br>";
        }

        public function displaycompte()
        {
                echo $this->getLIbelle() . $this->getTitulaire() .
                "le solde est de " . $this->getSoldeInitial() . " " . $this->getDevise() . "<br>";
        }

    
    }




?>

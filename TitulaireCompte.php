
<?php
    class TitulaireCompte {
        private string $_nom;
        private string $_prenom;
        private string $_date_naissance;
        private string $_ville;
        private array $_comptes;


    

    public function __construct(string $nom, string $prenom, string $date_naissance, string $ville)
    {
        $this->_nom = $nom;
        $this->_prenom = $prenom;
        $this->_date_naissance = $date_naissance;
        $this->_ville = $ville;
        $this->_comptes = [];

    }
        public function getNom(): string
        {
                return $this->_nom;
        }

        public function setNom(string $_nom): self
        {
                $this->_nom = $_nom;

                return $this;
        }


        public function getPrenom(): string
        {
                return $this->_prenom;
        }


        public function setPrenom(string $_prenom): self
        {
                $this->_prenom = $_prenom;

                return $this;
        }


        public function getDateNaissance(): string
        {
                return $this->_date_naissance;
        }


        public function setDateNaissance(string $_date_naissance): self
        {
                $this->_date_naissance = $_date_naissance;

                return $this;
        }

        public function getVille(): string
        {
                return $this->_ville;
        }

 
        public function setVille(string $_ville): self
        {
                $this->_ville = $_ville;

                return $this;
        }

        public function addCompte(CompteBancaire $compte)
        {
            $this->_comptes[] = $compte;
        }

        public function __toString()
        {
            return $this->_nom . $this->_prenom . $this->_ville ."\n";
        }

        public function getAge(){
            $date = new DateTime($this->_date_naissance);
            $now = new DateTime();
            $interval = $now->diff($date);
            return $interval->y;
        }

        public function displayTitulaire()
        {
            echo  $this->_prenom . " " . $this->_nom . " de " .$this->getAge() . " ans" . "\n" .
            "habitant à " . $this->_ville . " possède les comptes suivants : " . "\n";
            foreach ($this->_comptes as $compte) {
                echo $compte . "\n";
            }
        }

}

    

?>
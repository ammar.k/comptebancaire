
<h1>2 Exercice 2</h1>
<h2>Compte Bancaire</h2>

<?php

spl_autoload_register(function ($class_name) {
    require $class_name . '.php';
});

$titulaire1 = new TitulaireCompte("Doe", "John", "01/01/1970", "Paris");
$compte1 = new CompteBancaire("Compte courant", 1000, "€", $titulaire1);
$compte2 = new CompteBancaire("Livret A", 10000, "€", $titulaire1);

$titulaire1->displayTitulaire();
$compte1->crediter(1200);
$compte1->debiter(100);
$compte1->virement($compte2, 300);
$compte1->displayCompte();
$compte2->displayCompte();


?>